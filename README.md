# GeekDemo

#### 介绍
观看极客视频后写的demo

#### 软件架构
MVC 结构开发的 Demo


#### 开发要求
1.  xcode 13以上
2.  Swift 5
3.  CocoaPods 1.12.0

#### 使用说明

1.  pod install
2.  用 xcode 打开 GeekDemo.xcworkspace

#### 开发文档

1.  官方文档: https://docs.swift.org/swift-book/documentation/the-swift-programming-language/
2.  源码: https://github.com/apple/swift
3.  objc.io: https://www.objc.io/


#### 界面截图
<table style="width:100%;">
    <tr>
        <td>
            <img src="ScreenShots/登录1.png">
        </td>
        <td>
            <img src="ScreenShots/登录2.png">
        </td>
        <td>
            <img src="ScreenShots/首页.png">
        </td>
    </tr>
    <tr>
        <td>
            <img src="ScreenShots/详情.png">
        </td>
        <td>
            <img src="ScreenShots/我的.png">
        </td>
        <td>
            <img src="ScreenShots/已购.png">
        </td>
    </tr>
</table>