//
//  Tab.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/5/13.
//

import Foundation
import UIKit
import SnapKit

class Tab: UIView {

    var items: [String]
    var itemButtons: [UIButton]!
    var selectedItemButton: UIButton!
    
    var indicatorView: UIView!
    
    var selectedColor: UIColor? {
        didSet {
            if let color = self.selectedColor {
                self.indicatorView.backgroundColor = color
                itemButtons.forEach { button in
                    button.setTitleColor(color, for: .selected)
                }
            } else {
                self.indicatorView.backgroundColor = UIColor.primaryBgColor
                itemButtons.forEach { button in
                    button.setTitleColor(UIColor.primaryBgColor, for: .selected)
                }
            }
        }
    }
    var normalColor: UIColor?
    
    // 可失败的初始化器
    init?(items: [String]) {
        if items.count == 0 {
            return nil
        }
        self.items = items
        self.itemButtons = []
        super.init(frame: .zero)
        self.createViews()
    }
    
    private func createViews(){
        var lastView: UIView?
        for index in 0..<items.count {
            let button = UIButton(type: .custom)
            button.setTitle(items[index], for: .normal)
            button.setTitleColor(UIColor.fontColor, for: .normal)
            button.setTitleColor(UIColor.primaryBgColor, for: .selected)
            self.addSubview(button)
            if index == 0 {
                selectedItemButton = button
                selectedItemButton.isSelected = true
            }
            
            button.snp.makeConstraints { make in
                if index == 0 {
                    make.left.equalToSuperview()
                } else {
                    make.left.equalTo(lastView!.snp.right)
                    make.width.equalTo(lastView!)
                }
                make.top.bottom.equalToSuperview()
                
                if index == items.count-1 {
                    make.right.equalToSuperview()
                }
            }
            lastView = button
            button.addTarget(self, action: #selector(didClickButton(_:)), for: .touchUpInside)
            itemButtons.append(button)
        }
        indicatorView = UIView()
        indicatorView.backgroundColor = UIColor.primaryBgColor
        self.addSubview(indicatorView)
        indicatorView.snp.makeConstraints { make in
            make.centerX.equalTo(selectedItemButton)
            make.bottom.equalToSuperview()
            make.width.equalTo(80)
            make.height.equalTo(4)
        }
        
    }
    
    @objc func didClickButton(_ sender:UIButton) {
        guard sender != selectedItemButton else {
            return
        }
        
        selectedItemButton.isSelected = false
        sender.isSelected = true
        selectedItemButton = sender
        UIView.animate(withDuration: 3){
            self.indicatorView.snp.remakeConstraints{ make in
                make.centerX.equalTo(self.selectedItemButton)
                make.bottom.equalToSuperview()
                make.width.equalTo(80)
                make.height.equalTo(4)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
