//
//  CommonList.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/5/14.
//

import Foundation
import UIKit
import SnapKit

class CommonListCell<ItemType>: UITableViewCell {
    var item: ItemType?
    
    required override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol CommonListDelegate: AnyObject {
    func didSelectItem<CommonListItem>(_ item: CommonListItem)
}

class CommonList<ItemType, CellType: CommonListCell<ItemType>>: UIView, UITableViewDataSource, UITableViewDelegate {

    var tableView: UITableView
    var items: [ItemType] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    var reusableCellId = "cellId"
    var rowHeight: CGFloat = 120
    
    weak var delegate: CommonListDelegate?
    
    override init(frame: CGRect) {
        tableView = UITableView(frame: .zero, style: .plain)
        super.init(frame: frame)
        self.setupViews()
        print("CommonList init(frame:) invoked")
    }
    
    func setupViews(){
        tableView.dataSource = self
        tableView.delegate = self
        // tableView.tableFooterView = UIView()
        self.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: reusableCellId) as? CellType
        if cell == nil {
            cell = CellType(style: .subtitle, reuseIdentifier: reusableCellId)
        }
        cell?.item = items[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectItem(items[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
