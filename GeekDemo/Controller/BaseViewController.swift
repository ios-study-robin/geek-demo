//
//  BaseViewController.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/4/30.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 设置状态栏背景色
        UIApplication.statusBarBackgroundColor = UIColor.hexColor(0xffffff)
        setNeedsStatusBarAppearanceUpdate()
        
        view.backgroundColor = UIColor.hexColor(0xf2f4f7)
        // 设置默认返回按钮文字
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "返回", style: .plain, target: nil, action: nil)
        // 设置布局的延伸边界
        edgesForExtendedLayout = UIRectEdge(rawValue: 0)
    }
}
