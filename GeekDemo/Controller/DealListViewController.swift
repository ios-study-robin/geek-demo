//
//  DealListViewController.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/5/14.
//

import UIKit
import SnapKit

class DealListViewController: BaseViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "已购订单"

        let productList = CommonList<Deal, DealCell>()
        productList.items = FakeData.createDeals()
//        productList.delegate = self
        view.addSubview(productList)
        productList.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
        
    }
    
}
