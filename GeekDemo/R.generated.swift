//
// This is a generated file, do not edit!
// Generated by R.swift, see https://github.com/mac-cain13/R.swift
//

import Foundation
import RswiftResources
import UIKit

private class BundleFinder {}
let R = _R(bundle: Bundle(for: BundleFinder.self))

struct _R {
  let bundle: Foundation.Bundle
  var color: color { .init(bundle: bundle) }
  var image: image { .init(bundle: bundle) }
  var info: info { .init(bundle: bundle) }
  var storyboard: storyboard { .init(bundle: bundle) }

  func color(bundle: Foundation.Bundle) -> color {
    .init(bundle: bundle)
  }
  func image(bundle: Foundation.Bundle) -> image {
    .init(bundle: bundle)
  }
  func info(bundle: Foundation.Bundle) -> info {
    .init(bundle: bundle)
  }
  func storyboard(bundle: Foundation.Bundle) -> storyboard {
    .init(bundle: bundle)
  }
  func validate() throws {
    try self.storyboard.validate()
  }

  struct project {
    let developmentRegion = "en"
  }

  /// This `_R.color` struct is generated, and contains static references to 1 colors.
  struct color {
    let bundle: Foundation.Bundle

    /// Color `AccentColor`.
    var accentColor: RswiftResources.ColorResource { .init(name: "AccentColor", path: [], bundle: bundle) }
  }

  /// This `_R.image` struct is generated, and contains static references to 19 images.
  struct image {
    let bundle: Foundation.Bundle

    /// Image `book`.
    var book: RswiftResources.ImageResource { .init(name: "book", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `defaultAvatar`.
    var defaultAvatar: RswiftResources.ImageResource { .init(name: "defaultAvatar", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `detailBg`.
    var detailBg: RswiftResources.ImageResource { .init(name: "detailBg", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `home`.
    var home: RswiftResources.ImageResource { .init(name: "home", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `home_selected`.
    var home_selected: RswiftResources.ImageResource { .init(name: "home_selected", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_between`.
    var icon_between: RswiftResources.ImageResource { .init(name: "icon_between", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_cal`.
    var icon_cal: RswiftResources.ImageResource { .init(name: "icon_cal", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_column`.
    var icon_column: RswiftResources.ImageResource { .init(name: "icon_column", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_course`.
    var icon_course: RswiftResources.ImageResource { .init(name: "icon_course", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_document`.
    var icon_document: RswiftResources.ImageResource { .init(name: "icon_document", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_life`.
    var icon_life: RswiftResources.ImageResource { .init(name: "icon_life", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_live`.
    var icon_live: RswiftResources.ImageResource { .init(name: "icon_live", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_phone`.
    var icon_phone: RswiftResources.ImageResource { .init(name: "icon_phone", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_pwd`.
    var icon_pwd: RswiftResources.ImageResource { .init(name: "icon_pwd", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `icon_right_arrow`.
    var icon_right_arrow: RswiftResources.ImageResource { .init(name: "icon_right_arrow", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `logo`.
    var logo: RswiftResources.ImageResource { .init(name: "logo", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `mine`.
    var mine: RswiftResources.ImageResource { .init(name: "mine", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `mine_selected`.
    var mine_selected: RswiftResources.ImageResource { .init(name: "mine_selected", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }

    /// Image `student`.
    var student: RswiftResources.ImageResource { .init(name: "student", path: [], bundle: bundle, locale: nil, onDemandResourceTags: nil) }
  }

  /// This `_R.info` struct is generated, and contains static references to 1 properties.
  struct info {
    let bundle: Foundation.Bundle
    var uiApplicationSceneManifest: uiApplicationSceneManifest { .init(bundle: bundle) }

    func uiApplicationSceneManifest(bundle: Foundation.Bundle) -> uiApplicationSceneManifest {
      .init(bundle: bundle)
    }

    struct uiApplicationSceneManifest {
      let bundle: Foundation.Bundle

      let uiApplicationSupportsMultipleScenes: Bool = false

      var _key: String { bundle.infoDictionaryString(path: ["UIApplicationSceneManifest"], key: "_key") ?? "UIApplicationSceneManifest" }
      var uiSceneConfigurations: uiSceneConfigurations { .init(bundle: bundle) }

      func uiSceneConfigurations(bundle: Foundation.Bundle) -> uiSceneConfigurations {
        .init(bundle: bundle)
      }

      struct uiSceneConfigurations {
        let bundle: Foundation.Bundle
        var _key: String { bundle.infoDictionaryString(path: ["UIApplicationSceneManifest", "UISceneConfigurations"], key: "_key") ?? "UISceneConfigurations" }
        var uiWindowSceneSessionRoleApplication: uiWindowSceneSessionRoleApplication { .init(bundle: bundle) }

        func uiWindowSceneSessionRoleApplication(bundle: Foundation.Bundle) -> uiWindowSceneSessionRoleApplication {
          .init(bundle: bundle)
        }

        struct uiWindowSceneSessionRoleApplication {
          let bundle: Foundation.Bundle
          var defaultConfiguration: defaultConfiguration { .init(bundle: bundle) }

          func defaultConfiguration(bundle: Foundation.Bundle) -> defaultConfiguration {
            .init(bundle: bundle)
          }

          struct defaultConfiguration {
            let bundle: Foundation.Bundle
            var uiSceneConfigurationName: String { bundle.infoDictionaryString(path: ["UIApplicationSceneManifest", "UISceneConfigurations", "UIWindowSceneSessionRoleApplication"], key: "UISceneConfigurationName") ?? "Default Configuration" }
            var uiSceneDelegateClassName: String { bundle.infoDictionaryString(path: ["UIApplicationSceneManifest", "UISceneConfigurations", "UIWindowSceneSessionRoleApplication"], key: "UISceneDelegateClassName") ?? "$(PRODUCT_MODULE_NAME).SceneDelegate" }
          }
        }
      }
    }
  }

  /// This `_R.storyboard` struct is generated, and contains static references to 1 storyboards.
  struct storyboard {
    let bundle: Foundation.Bundle
    var launchScreen: launchScreen { .init(bundle: bundle) }

    func launchScreen(bundle: Foundation.Bundle) -> launchScreen {
      .init(bundle: bundle)
    }
    func validate() throws {
      try self.launchScreen.validate()
    }


    /// Storyboard `LaunchScreen`.
    struct launchScreen: RswiftResources.StoryboardReference, RswiftResources.InitialControllerContainer {
      typealias InitialController = UIKit.UIViewController

      let bundle: Foundation.Bundle

      let name = "LaunchScreen"
      func validate() throws {

      }
    }
  }
}