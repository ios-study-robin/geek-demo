//
//  Deal.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/4/30.
//

import Foundation

struct Deal {
    var product: Product
    var progress: Int
}
