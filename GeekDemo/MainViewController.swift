//
//  MainViewController.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/5/1.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let homeVC = HomeViewController()
//        homeVC.tabBarItem.image = R.image.home()
//        homeVC.tabBarItem.selectedImage = R.image.home_selected()
//        homeVC.tabBarItem.setTitleTextAttributes(, for: .selected)
//        homeVC.tabBarItem.title = "首页"
//        let navigationHomeVC = UINavigationController(rootViewController: homeVC)
//        addChild(navigationHomeVC)
//
//
//        let mineVC = HomeViewController()
//        mineVC.tabBarItem.image = R.image.mine()
//        mineVC.tabBarItem.selectedImage = R.image.mine_selected()
//        mineVC.tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.fontColor], for: .selected)
//        mineVC.tabBarItem.title = "我的"
//        let navigationMineVC = UINavigationController(rootViewController: mineVC)
//        addChild(navigationMineVC)
        
//        // 设置图标颜色
//        tabBar.tintColor = UIColor.fontColor
        
        
        // 优化代码如下：
        // 设置菜单
        setupTabbarItems()
        
    }
    
    func setupTabbarItems(){
        let tabBarList: [[String: Any?]] = [
            [
                "vc": HomeViewController(),
                "image": R.image.home(),
                "selectedImage": R.image.home_selected(),
                "title": "首页",
            ],
            [
                "vc": MineViewController(),
                "image": R.image.mine(),
                "selectedImage": R.image.mine_selected(),
                "title": "我的",
            ]
        ]
        
        for item in tabBarList {
            if let vc = item["vc"] as? UIViewController, let tabBarItem = vc.tabBarItem {
                tabBarItem.image = item["image"] as? UIImage
                tabBarItem.selectedImage = item["selectedImage"] as? UIImage
                tabBarItem.title = item["title"] as? String
                
                // 下行代码只改变文字颜色，建议使用 tabBar.tintColor = UIColor.fontColor
                // tabBarItem.setTitleTextAttributes([.foregroundColor: UIColor.fontColor], for: .selected)
                
                // 创建 UINavigationController
                let navigationVC = UINavigationController(rootViewController: vc)
                // 设置导航头背景色，两行代码一起用，防止不生效
                navigationVC.navigationBar.backgroundColor = UIColor.white
                navigationVC.navigationBar.setBackgroundImage(UIColor.white.toImage(), for: .default)
                
                // 添加到主 VC 中
                addChild(navigationVC)
            }
        }
        
        // 图片文字一起变色
        tabBar.tintColor = UIColor.fontColor
        
        // 设置背景色，两行代码一起用，防止不生效
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().backgroundImage = UIColor.white.toImage()
        
        //默认选中的是第1个视图
        selectedIndex = 0
    }
    
}
