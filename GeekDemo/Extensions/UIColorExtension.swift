//
//  UIColorExtension.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/4/26.
//

import Foundation
import UIKit

extension UIColor {
    // color
    static let fontColor = UIColor.hexColor(0x333333)
    static let fontSecondaryColor = UIColor.hexColor(0x999999)
    static let fontThirdColor = UIColor.hexColor(0xe23b41)
    static let borderColor = UIColor.fontColor
    static let primaryBgColor = UIColor.hexColor(0xf8892e)
    static let activeBgColor = UIColor.hexColor(0xff0000)
    
    
    static func hexColor(_ hexValue: Int, alphaValue: Float) -> UIColor {
        return UIColor(red: CGFloat((hexValue & 0xFF0000) >> 16) / 255, green: CGFloat((hexValue & 0x00FF00) >> 8) / 255, blue: CGFloat(hexValue & 0x0000FF) / 255, alpha: CGFloat(alphaValue))
    }
    
    static func hexColor(_ hexValue: Int) -> UIColor {
        return hexColor(hexValue, alphaValue: 1)
    }
    
    convenience init(_ hexValue: Int, alphaValue: Float) {
        self.init(red: CGFloat((hexValue & 0xFF0000) >> 16) / 255, green: CGFloat((hexValue & 0x00FF00) >> 8) / 255, blue: CGFloat(hexValue & 0x0000FF) / 255, alpha: CGFloat(alphaValue))
    }
    
    convenience init(_ hexValue: Int) {
        self.init(hexValue, alphaValue: 1)
    }
    
    func toImage() -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(self.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}

