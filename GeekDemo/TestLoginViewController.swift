//
//  TestLoginViewController.swift
//  GeekDemo
//
//  Created by ROBIN.J.Y.ZHONG on 2023/4/25.
//

import UIKit
import SnapKit

class TestLoginViewController: BaseViewController
    // UITabBarController, UITabBarControllerDelegate
{

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "首页"
        
        let navLoginBtn = UIButton(type: .custom)
        navLoginBtn.setTitle("登录", for: .normal)
        navLoginBtn.setTitleColor(UIColor.fontColor, for: .normal)
        navLoginBtn.setBackgroundImage(UIColor.primaryBgColor.toImage(), for: .normal)
        navLoginBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        view.addSubview(navLoginBtn)
        navLoginBtn.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(30)
            make.height.equalTo(50)
        }
        
        navLoginBtn.addTarget(self, action: #selector(navLogin), for: .touchUpInside)
    }
    
    @objc func navLogin(){
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }

}

