//
//  GeekDemoTests.swift
//  GeekDemoTests
//
//  Created by ROBIN.J.Y.ZHONG on 2023/7/5.
//

import XCTest

// 导入需要测试的模块名
@testable import GeekDemo

final class GeekDemoTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // 这里测 Model
    func testModel() throws {
        let testProduct = Product(imageUrl: "https://static001.geekbang.org/resource/image/87/ee/8778de4ccd67425a762cea15361639ee.jpg?x-oss-process=image/resize,m_fill,h_336,w_254", name: "MongoDB高手课", desc: "Tapdata CTO、MongoDB中文社区主席、前MongoDB大中华区首席架构师", price: 129, teacher: "唐建法", total: 47, update: 9, studentCount: 2205, detail: "MongoDB 是当前广受欢迎的 NoSQL 数据库，目前社...", courseList: "第一章：MongoDB再入门 (9讲)")
        let testDeal = Deal(product: testProduct, progress: 10)
        
        // 断言
        XCTAssertNotNil(testDeal)
        XCTAssertEqual(testProduct.imageUrl, "https://static001.geekbang.org/resource/image/87/ee/8778de4ccd67425a762cea15361639ee.jpg?x-oss-process=image/resize,m_fill,h_336,w_254")
        XCTAssertTrue(testDeal.progress == 10)
        XCTAssertFalse(testDeal.progress != 10)
        XCTAssertLessThan(testDeal.progress, 20)
        XCTAssertGreaterThanOrEqual(testDeal.progress, 1)
        // 还有更多断言相关内容
        // 通用断言
        // XCTAssert(expression, format...)当expression求值为TRUE时通过
        
    }

}
